# CCWatM

Climate-CWatM (CCWatM) - A simplified version of the Community Water Model (CWatM) to enable direct coupling to climate models.

## DONE

- Include flag in settingsfile to turn off paddy irrigation. If turned off, paddy fields will be treated as nonpaddy fields.

## TODO

- Check which modules are not necessary anymore, remove those modules
- Remove unnecessary code from existing modules