# CCWatM

Climate-CWatM (CCWatM) - A simplified version of the Community Water Model (CWatM) to enable direct coupling to climate models.

## TODO

- Include representation of Paddy irrigation
- Check which modules are not necessary anymore, remove those modules
- Remove unnecessary code from existing modules