# -------------------------------------------------------------------------
# Name:        Soil module
# Purpose:
#
# Author:      Peter Greve, Peter Burek
#
# Created:     15/07/2016
# Copyright:   (c) Peter Burek 2016, (c) Peter Greve 2023 
# -------------------------------------------------------------------------

from cwatm.management_modules.data_handling import *


class soil(object):

    """
    **SOIL INITIAL**


    **Global variables**

    =====================================  ======================================================================  =====
    Variable [self.var]                    Description                                                             Unit 
    =====================================  ======================================================================  =====
    capRiseFrac                            fraction of a grid cell where capillar rise may happen                  m    
    modflow                                Flag: True if modflow_coupling = True in settings file                  --   
    snowEvap                               total evaporation from snow for a snow layers                           m    
    fracCrops_nonIrr                       Fraction of cell currently planted with specific non-irr crops          --   
    currentKC                              Current crop coefficient for specific crops                             --   
    weighted_KC_Irr_woFallow_fullKc                                                                                --   
    weighted_KC_Irr_woFallow                                                                                       --   
    storGroundwater                        Groundwater storage (non-fossil). This is primarily used when not usin  m    
    includeCrops                           1 when includeCrops=True in Settings, 0 otherwise                       bool 
    Crops                                  Internal: List of specific crops and Kc/Ky parameters                   --   
    potTranspiration                       Potential transpiration (after removing of evaporation)                 m    
    interceptEvap                          simulated evaporation from water intercepted by vegetation              m    
    cropKC                                 crop coefficient for each of the 4 different land cover types (forest,  --   
    topwater                               quantity of water above the soil (flooding)                             m    
    minCropKC                              minimum crop factor (default 0.2)                                       --   
    availWaterInfiltration                 quantity of water reaching the soil after interception, more snowmelt   m    
    rootDepth                                                                                                      --   
    KSat1                                                                                                          --   
    KSat2                                                                                                          --   
    KSat3                                                                                                          --   
    genuM1                                                                                                         --   
    genuM2                                                                                                         --   
    genuM3                                                                                                         --   
    genuInvM1                                                                                                      --   
    genuInvM2                                                                                                      --   
    genuInvM3                                                                                                      --   
    ws1                                    Maximum storage capacity in layer 1                                     m    
    ws2                                    Maximum storage capacity in layer 2                                     m    
    ws3                                    Maximum storage capacity in layer 3                                     m    
    wres1                                  Residual storage capacity in layer 1                                    m    
    wres2                                  Residual storage capacity in layer 2                                    m    
    wres3                                  Residual storage capacity in layer 3                                    m    
    wrange1                                                                                                        --   
    wrange2                                                                                                        --   
    wrange3                                                                                                        --   
    wfc1                                   Soil moisture at field capacity in layer 1                              --   
    wfc2                                   Soil moisture at field capacity in layer 2                              --   
    wfc3                                   Soil moisture at field capacity in layer 3                              --   
    wwp1                                   Soil moisture at wilting point in layer 1                               --   
    wwp2                                   Soil moisture at wilting point in layer 2                               --   
    wwp3                                   Soil moisture at wilting point in layer 3                               --   
    kunSatFC12                                                                                                     --   
    kunSatFC23                                                                                                     --   
    arnoBeta                                                                                                       --   
    adjRoot                                                                                                        --   
    maxtopwater                            maximum heigth of topwater                                              m    
    cellArea                               Area of cell                                                            m2   
    EWRef                                  potential evaporation rate from water surface                           m    
    FrostIndexThreshold                    Degree Days Frost Threshold (stops infiltration, percolation and capil  --   
    FrostIndex                             FrostIndex - Molnau and Bissel (1983), A Continuous Frozen Ground Inde  --   
    potBareSoilEvap                        potential bare soil evaporation (calculated with minus snow evaporatio  m    
    irr_Paddy_month                                                                                                --   
    fracCrops_Irr                          Fraction of cell currently planted with specific irrigated crops        %    
    actTransTotal_month_nonIrr             Internal variable: Running total of  transpiration for specific non-ir  m    
    actTransTotal_month_Irr                Internal variable: Running total of  transpiration for specific irriga  m    
    irr_crop_month                                                                                                 --   
    frac_totalIrr                          Fraction sown with specific irrigated crops                             %    
    weighted_KC_nonIrr_woFallow                                                                                    --   
    totalPotET                             Potential evaporation per land use class                                m    
    actualET                               simulated evapotranspiration from soil, flooded area and vegetation     m    
    soilLayers                             Number of soil layers                                                   --   
    soildepth                              Thickness of the first soil layer                                       m    
    w1                                     Simulated water storage in the layer 1                                  m    
    w2                                     Simulated water storage in the layer 2                                  m    
    w3                                     Simulated water storage in the layer 3                                  m    
    directRunoff                           Simulated surface runoff                                                m    
    interflow                              Simulated flow reaching runoff instead of groundwater                   m    
    openWaterEvap                          Simulated evaporation from open areas                                   m    
    actTransTotal                          Total actual transpiration from the three soil layers                   m    
    actBareSoilEvap                        Simulated evaporation from the first soil layer                         m    
    percolationImp                         Fraction of area covered by the corresponding landcover type            m    
    cropGroupNumber                        soil water depletion fraction, Van Diepen et al., 1988: WOFOST 6.0, p.  --   
    cPrefFlow                              Factor influencing preferential flow (flow from surface to GW)          --   
    pumping_actual                                                                                                 --   
    gwdepth_observations                   Input, gw_depth_observations, groundwater depth observations            m    
    gwdepth_adjuster                       Groundwater depth adjuster                                              m    
    rws                                    Transpiration reduction factor (in case of water stress)                --   
    prefFlow                               Flow going directly from soil surface to groundwater [land class speci  m    
    infiltration                           Water actually infiltrating the soil                                    m    
    capRiseFromGW                          Simulated capillary rise from groundwater                               m    
    NoSubSteps                             Number of sub steps to calculate soil percolation                       --   
    perc1to2                               Simulated water flow from soil layer 1 to soil layer 2                  m    
    perc2to3                               Simulated water flow from soil layer 2 to soil layer 3                  m    
    perc3toGW                              Simulated water flow from soil layer 3 to groundwater                   m    
    theta1                                 fraction of water in soil compartment 1 for each land use class         --   
    theta2                                 fraction of water in soil compartment 2 for each land use class         --   
    theta3                                 fraction of water in soil compartment 3 for each land use class         --   
    actTransTotal_forest                   Transpiration from forest land cover                                    m    
    actTransTotal_grasslands               Transpiration from grasslands land cover                                m    
    actTransTotal_paddy                    Transpiration from paddy land cover                                     m    
    actTransTotal_nonpaddy                 Transpiration from non-paddy land cover                                 m    
    actTransTotal_crops_Irr                Transpiration associated with specific irrigated crops                  m    
    actTransTotal_crops_nonIrr             Transpiration associated with specific non-irr crops                    m    
    irr_crop                                                                                                       --   
    irrM3_crop_month_segment                                                                                       --   
    irrM3_Paddy_month_segment                                                                                      --   
    gwRecharge                             groundwater recharge                                                    m    
    baseflow                               simulated baseflow (= groundwater discharge to river)                   m    
    capillar                               Flow from groundwater to the third CWATM soil layer. Used with MODFLOW  m    
    capriseindex                                                                                                   --   
    soildepth12                            Total thickness of layer 2 and 3                                        m    
    fracVegCover                           Fraction of specific land covers (0=forest, 1=grasslands, etc.)         %    
    adminSegments                          Domestic agents                                                         Int  
    act_irrConsumption                     actual irrigation water consumption                                     m    
    act_irrNonpaddyWithdrawal              non-paddy irrigation withdrawal                                         m    
    act_irrPaddyWithdrawal                 paddy irrigation withdrawal                                             m    
    =====================================  ======================================================================  =====

    **Functions**
    """

    def __init__(self, model):
        self.var = model.var
        self.model = model

    def initial(self):
        """
        Initial part of the soil module

        * Initialize all the hydraulic properties of soil
        * Set soil depth

        """

        self.var.soilLayers = 3
        # --- Topography -----------------------------------------------------
        # maps of relative elevation above flood plains
        dzRel = ['dzRel0001','dzRel0005',
                 'dzRel0010','dzRel0020','dzRel0030','dzRel0040','dzRel0050',
                 'dzRel0060','dzRel0070','dzRel0080','dzRel0090','dzRel0100']
        for i in dzRel:
            vars(self.var)[i] = readnetcdfWithoutTime(cbinding('relativeElevation'),i)

        # Fraction of area where percolation to groundwater is impeded [dimensionless]
        self.var.percolationImp = np.maximum(0,np.minimum(1,loadmap('percolationImp') * loadmap('factor_interflow')))


    # ------------ Preferential Flow constant ------------------------------------------
        self.var.cropGroupNumber = loadmap('cropgroupnumber')
        # soil water depletion fraction, Van Diepen et al., 1988: WOFOST 6.0, p.86, Doorenbos et. al 1978
        # crop groups for formular in van Diepen et al, 1988

    # ------------ Preferential Flow constant ------------------------------------------
        self.var.cPrefFlow = loadmap('preferentialFlowConstant')


    # ------------ SOIL DEPTH ----------------------------------------------------------
        # soil thickness and storage

        #soilDepthLayer = [('soildepth', 'SoilDepth'),('storCap','soilWaterStorageCap')]
        soilDepthLayer = [('soildepth', 'SoilDepth')]
        for layer,property  in soilDepthLayer:
            vars(self.var)[layer] = np.tile(globals.inZero, (self.var.soilLayers, 1))

        # first soil layer = 5 cm
        self.var.soildepth[0] = 0.05 + globals.inZero
        # second soul layer minimum 5cm
        self.var.soildepth[1] = np.maximum(0.05, loadmap('StorDepth1') - self.var.soildepth[0])

        # soil depth[1] is inc/decr by a calibration factor
        #self.var.soildepth[1] =  self.var.soildepth[1] * loadmap('soildepth_factor')
        #self.var.soildepth[1] = np.maximum(0.05, self.var.soildepth[1])

        # corrected by the calibration factor, total soil depth stays the same
        #self.var.soildepth[2] = loadmap('StorDepth2') + (1. - loadmap('soildepth_factor') * self.var.soildepth[1])
        #self.var.soildepth[2] = loadmap('StorDepth2') * loadmap('soildepth_factor')
        self.var.soildepth[2] = loadmap('StorDepth2')
        self.var.soildepth[2] = np.maximum(0.05, self.var.soildepth[2])

        # Calibration
        soildepth_factor =  loadmap('soildepth_factor')
        self.var.soildepth[1] = self.var.soildepth[1] * soildepth_factor
        self.var.soildepth[2] = self.var.soildepth[2] * soildepth_factor
        self.var.soildepth12 = self.var.soildepth[1] + self.var.soildepth[2]

        ii= 0

        # report("C:/work/output2/soil.map", self.var.soildepth12)



        # This is here, as groundwater.py is not called if MODFLOW is used
        self.var.pumping_actual = globals.inZero.copy()
        self.var.capillar = globals.inZero.copy()
        self.var.baseflow = globals.inZero.copy()

        if 'gw_depth_observations' in binding:
            self.var.gwdepth_observations = readnetcdfWithoutTime(cbinding('gw_depth_observations'),
                                                                  value='Groundwater depth')
        if 'gw_depth_sim_obs' in binding:
            self.var.gwdepth_adjuster = loadmap('gw_depth_sim_obs')




